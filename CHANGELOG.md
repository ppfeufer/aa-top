# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased] - yyyy-mm-dd

### Added

### Changed

### Fixed

## [0.3.0a] - 2023-11-08

### Added

### Changed

### Fixed

- Only output memory usage if MariaDB/MySQL is installed
- DB size was off by a factor of 1024 * 1024
- Updated readme with the file permissions needed

## [0.2.0a] - 2023-10-08

### Added

- More memory and disk space details

### Changed

### Fixed

- aatop.txt will now update in both development debug mode and production
- in the case cpu frequency max cannot be detected, use current

## [0.1.0a] - 2023-10-08

Initial Release
App Maintenance
